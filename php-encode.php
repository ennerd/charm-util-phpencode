<?php
namespace Charm;

/**
 * A compact version of var_export($value, true) which returns array
 * in the short format using [] instead of array()
 */
function php_encode($value, bool $pretty=false) {
    $enc = @var_export($value, true);
    $re = <<<'RE'
        /('(\\[\\']|[^'\\]+|[^'])*')|object\)|array ?\(|\)\)|\)|\s+/
        RE;
    return preg_replace_callback($re, function($m) use ($pretty) {
        switch ($m[0]) {
            case 'object)': return $m[0];
            case '))': return '])';
            case ')': return ']';
            case 'array(':
            case 'array (':
                return '[';
        }
        if ($m[0][0] === "'") return $m[0];
        // whitespace
        if ($pretty) return $m[0];
        return '';
    }, $enc);
}
