charm/util-phpencode
====================

`php_encode` - Outputs or returns a parsable string representation of a variable, similar to
the built in `var_export()` function but with a more modern syntax with short array notation.


Description
-----------

```
Charm\php_encode(mixed $value, bool $pretty = false): string
```

This function returns valid PHP code which contains a static version of the encoded value,
and can be cached by the PHP opcache.


Parameters
----------

### value

The value you want to serialize

### pretty

A boolean indicating if you wish a version with more whitespace and proper indentation.


Return Values
-------------

Returns the variable representation as a string.


Examples
--------

```
use function Charm\php_encode;

echo php_encode($object);
// output: '(object)["property"=>"value","number"=>42,"array"=>[1,2,3]]'
```

