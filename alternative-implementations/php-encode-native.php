<?php
namespace Charm;

/**
 * Encode a PHP value into a compact array.
 */
function _php_encode_using_var_export($value) {
    static $map = [
        "\0" => "\\x00",
        "\x01" => "\\x01",
        "\x02" => "\\x02",
        "\x03" => "\\x03",
        "\x04" => "\\x04",
        "\x05" => "\\x05",
        "\x06" => "\\x06",
        "\x07" => "\\x07",
        "\x08" => "\\x08",
        "\x09" => "\\x09",
        "\x0A" => "\\x0A",
        "\x0B" => "\\x0B",
        "\x0C" => "\\x0C",
        "\x0D" => "\\x0D",
        "\x0E" => "\\x0E",
        "\x0F" => "\\x0F",
        "\x1B" => "\\x1B",
        "\x7F" => "\\x7F",
        "\a" => "\\a",
        "\f" => "\\f",
        "\r" => "\\r",
        "\t" => "\\t",
        '\\' => '\\\\',
        '"' => "\\\"",
        "\n" => "\\n",
        '$' => '\\$',
        "{" => "\\x7B",
    ];

    if ($value === false) return 'false';
    if ($value === true) return 'true';
    if ($value === null) return 'null';
    if (is_int($value) || is_float($value)) {
        return (string) $value;
    }
    if (is_string($value)) {
        return '"'.strtr($value, $map).'"';
    }
    if (is_array($value) || is_object($value)) {
        $i = 0;
        $res = '';
        foreach ((array) $value as $k => $v) {
            if ($v === false) $v = 'false';
            elseif ($v === true) $v = 'true';
            elseif ($v === null) $v = 'null';
            elseif (is_int($v) || is_float($v));
            elseif (is_string($v)) $v = '"'.strtr($v, $map).'"';
            else $v = php_encode($v);
            if ($k === $i) {
                $res .= $v.',';
                $i++;
            } elseif (is_numeric($k)) {
                $i = null;
                $res .= "$k=>$v,";
            } else {
                $i = null;
                $res .= '"'.strtr($k, $map)."\"=>$v,";
            }
        }
        return "[$res]";
    }
    return var_export($value, true);
}

