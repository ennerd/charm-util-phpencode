<?php
namespace Charm;

/**
 * Returns a compact PHP-array representation of a value. The function does not preserve any
 * dynamic properties of the value and is like a PHP compatible version of `json_encode`.
 *
 * @param mixed $value The value to encode
 * @param bool $pretty Return a multi-line indented version of the string.
 */
function php_encode($value, bool $pretty=false): string {
    // captures all braces outside of quoted string literals

    $re = '/(?<key>"\d+":\s?)|"(\\[\\"]|[^"])*"|[{}[\]]|:\s?/';
    $encoded1 = json_encode(
        $value,
        ($pretty ? JSON_PRETTY_PRINT : 0) |
        JSON_UNESCAPED_SLASHES |
        JSON_PRESERVE_ZERO_FRACTION |
        JSON_UNESCAPED_LINE_TERMINATORS
    );
    if ($encoded1 === false) {
        /**
         * If json_encode fails, we fall back to encoding parts of the array manually
         */
        if (is_array($value) || is_object($value)) {
            $parts = [];
            $i = 0;
            foreach ((array) $value as $k => $v) {
                if ($k === $i) {
                    $parts[] = php_encode($value);
                    $i++;
                } else {
                    $parts[] = var_export($k, true)."=>".php_encode($v);
                    $i = null;
                }
            }
            return "[".implode(",", $parts)."]";
        } elseif (is_string($value)) {
            $map = [
                "\0" => "\\x00",
                "\x01" => "\\x01",
                "\x02" => "\\x02",
                "\x03" => "\\x03",
                "\x04" => "\\x04",
                "\x05" => "\\x05",
                "\x06" => "\\x06",
                "\x07" => "\\x07",
                "\x08" => "\\x08",
                "\x09" => "\\x09",
                "\x0A" => "\\x0A",
                "\x0B" => "\\x0B",
                "\x0C" => "\\x0C",
                "\x0D" => "\\x0D",
                "\x0E" => "\\x0E",
                "\x0F" => "\\x0F",
                "\x1B" => "\\x1B",
                "\x7F" => "\\x7F",
                "\a" => "\\a",
                "\f" => "\\f",
                "\r" => "\\r",
                "\t" => "\\t",
                '\\' => '\\\\',
                '"' => "\\\"",
                "\n" => "\\n",
                '$' => '\\$',
                "{" => "\\x7B",
            ];
            return '"'.strtr($value, $map).'"';
        } else {
            return var_export($value, true);
        }
    }

    // the stack is used to remove array key values which PHP will infer
    $stack = [];
    $stackLength = -1;
    return preg_replace_callback($re, function($matches) use (&$stack, &$stackLength) {
        if ($matches[0] === "{") {
            $stack[++$stackLength] = 0;
            return "[";
        }
        if ($matches[0] === "}") {
            --$stackLength;
            return "]";
        }
        if ($matches[0] === ':') {
            return '=>';
        }
        if ($matches[0][0] === '"') {
            if ((isset($matches['key']) || key_exists('key', $matches)) && "" !== $matches['key']) {
                $id = intval(substr($matches['key'], 1, -1));
                if ($stack[$stackLength] === $id) {
                    return '';
                } else {
                    return $id;
                }
            }
            if (is_int(strpos($matches[0], "\\"))) {
                $s = json_decode($matches[0]);
                return var_export($s, true);
            }
        }
        if ($matches[0][0] === ':') {
            return '=>';
        }
        return $matches[0];
    }, $encoded1);
    return $encoded2;
}


