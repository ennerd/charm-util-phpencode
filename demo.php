<?php
require("php-encode.php");

use function Charm\php_encode;

$phpValue = [
    "some" => "array",
    "with" => [
        "nested" => "arrays",
        "and_numbers" => 0.0,
        "and varying" => (object) [ "complexity", 1, true, false ],
    ],
    "ex" => new Exception("Hello World!"),
];

echo "Using php_encode(\$value);\n";
echo "==========================\n";
echo "\nRunning 50 000 iterations\n\n";
$t = microtime(true);
for ($i = 0; $i < 50000; $i++) {
    $encodedString = php_encode($phpValue);
}
echo "Encoded string:\n".$encodedString."\n";
$t = microtime(true) - $t;
echo "Average time per encode: $t milliseconds, or ".(50000/$t)." per second.\n\n";


echo "Using php_encode(\$value, true);\n";
echo "================================\n";
echo "\nRunning 50 000 iterations\n\n";
$t = microtime(true);
for ($i = 0; $i < 50000; $i++) {
    $encodedString = php_encode($phpValue, true);
}
echo "Pretty encoded string:\n".$encodedString."\n";
$t = microtime(true) - $t;
echo "Average time per encode: $t milliseconds, or ".(50000/$t)." per second.\n\n";


echo "Using json_encode(\$value);\n";
echo "================================\n";
echo "\nRunning 50 000 iterations\n\n";
$t = microtime(true);
for ($i = 0; $i < 50000; $i++) {
    $encodedString = json_encode($phpValue);
}
echo "Pretty encoded string:\n".$encodedString."\n";
$t = microtime(true) - $t;
echo "Average time per encode: $t milliseconds, or ".(50000/$t)." per second.\n\n";


