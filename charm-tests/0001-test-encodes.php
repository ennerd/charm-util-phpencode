<?php return [
    "Test various encodings of PHP values",
    function ($arg) {
        return \Charm\php_encode($arg[0], $arg[1]);
    },
    [ 123, "123" ]
];
